const { Transform } = require('stream');
// Create a new transform stream class that can validate files.
class FileValidator extends Transform {
  constructor(options) {
    super(options.streamOptions);
    this.maxFileSize = options.maxFileSize;
    this.totalBytesInBuffer = 0;
  }
  _transform (chunk, encoding, callback) {
    this.totalBytesInBuffer += chunk.length;
    // Look to see if the file size is too large.
    if (this.totalBytesInBuffer > this.maxFileSize) {
      const err = new Error(`The file size exceeded the limit of ${this.maxFileSize} bytes`);
      err.code = 'MAXFILESIZEEXCEEDED';
      callback(err);
      return;
    }
    this.push(chunk);
    callback(null);
  }
  _flush (done) {
    done();
  }
}
module.exports = FileValidator;