const env = process.env.NODE_ENV || 'development'
var fs = require('fs');
const config = require(`./../../config/${env}.js`)
const PYTHON_SERVER_URL = config.PYTHON_SERVER_URL
const axios = require('axios')
const ExamLogModel = require('../models/examSchedule')
const UserChatModel = require('../models/userChat')
const helper = require('../helpers/helper')
var spawn 
= require('child_process').spawn;
exports.objectDetection = (req, res) => {
  res.json({ status: 200, message: 'person detected.' })
}

exports.faceDetection = (req, res) => {
  try {
    axios.post(`http://134.209.164.157:5001/face`, req.body).then(
      response => {
        ExamLogModel.addExamLogs(req.body, response.data)
          .then(data => {
            
            return res.json({ status: true, data: response.data })
          })
          .catch(err => {
            console.log('ERROR : Failed to insert exam logs', err)
            return res.json({ status: true, data: response.data })
          })
      },
      error => {
        return res.json({ status: false, data: error })
      }
    )
  } catch (error) {
    return res.json({ status: false, data: null })
  }
}

exports.uploadVideo = (req, res) => {
  try {
    const { data, fileName, examId, userId, camId} = req.body;
    const dataBuffer = new Buffer(data, 'base64');
    var dir = `${__basedir}/public/`;
    if (!fs.existsSync(dir)){
        fs.mkdirSync(dir, 0777);
    }
    const fileStream = fs.createWriteStream(`${__basedir}/public/${fileName}`, { flags: 'a', encoding: "utf8",
    mode: parseInt('0644', 8) })
    fileStream.write(dataBuffer)
    fileStream.on('finish', () => {
     // console.log("Stream Closed", dataBuffer);
      
    });
    fileStream.end();
    return res.json({'status':true, 'uploadData':true});
    
  } catch (error) {
    //console.log('catch --x', error)
    return res.json({ gotit: false })
  }
}

exports.sendExamVideo = (req, res) => {
  try{
      let fileName = `usr_${req.body.fileName}_.mp4`;
      return res.json({
        fileName:fileName,
        success:true,
        fileLocation:`/public/${fileName}`
      });
  }catch(error){

  }
}

exports.downloadVideo = (req, res) => {
  try{
    let fileName = `usr_${req.body.fileName}_.mp4`;
    res.setHeader('Content-disposition', `attachment; filename=${fileName}`);
    return res.sendFile(`/public/${fileName}`, {root:__basedir});
  }catch(error){

  }
}
exports.uploadSound = (req, res) => {
  try {
    try {
      axios.post(`${PYTHON_SERVER_URL}/audio`, req.body).then(
        response => {
          ExamLogModel.addExamLogs(req.body, response.data.message)
            .then(data => {
              return res.json({ data: response.data })
            })
            .catch(err => {
              return res.json({ data: response.data })
            })
        },
        error => {
          console.log(error)
          return res.json({ status: false, data: error })
        }
      )
    } catch (error) {
      console.log('catch --y', error)
      return res.json({ status: false, data: null })
    }
  } catch (error) {
    console.log('catch --x', error)
    return res.json({ gotit: false })
  }
}

exports.ffmpeg = (req, res) => {
  try {
    var spawn = require('child_process').spawn
    var cmd = 'ffmpeg'
    var args = [
      '-y',
      '-i',
      `video=${req.body.deviceName}`,
      '-codec:a',
      'aac',
      '-strict',
      'experimental',
      '-b:a',
      '96k',
      '-vcodec',
      'libx264',
      '-bufsize',
      '1000k',
      '-preset',
      'superfast',
      '-s',
      '600x400',
      '-ac',
      '2',
      '-r',
      '15',
      '-b:v',
      '1000k',
      '-c:v',
      'h264',
      '-f',
      'flv',
      `rtmp://35.158.105.212:1935/headend/heman/${req.body.examId}`
    ];

    var proc = spawn(cmd, args)

    proc.stdout.on('data', function (data) {
     
    })

    proc.stderr.setEncoding('utf8')
    proc.stderr.on('data', function (data) {
     
    })

    proc.on('close', function () {
      
    })
  } catch (error) {
    console.log('catch --x', error)
    return res.json({ status: false, data: null })
  }
}

/**
 * stream capturing logs for audio
 */
exports.soundStream = (req, res) => {
  try {
    // axios.post('http://134.122.20.158:5001/audio', req.body).then(
      axios.post('http://134.209.164.157:5001/audio', req.body).then(
      response => {
		
        ExamLogModel.addExamLogs(req.body, response.data)
          .then(data => {
            return res.json({ data: response.data })
          })
          .catch(err => {
            return res.json({ data: response.data })
          })
      },
      error => {
        return res.json({ status: false, data: error })
      }
    )
  } catch (error) {
    return res.json({ status: false, data: null })
  }
}

/**
 * stream capturing logs for objects
 */
exports.videoStream = (req, res) => {
  try {
    // axios.post('http://134.122.20.158:5001/object', req.body).then(
    //console.log("spawn -- storing camId", camId);
	
    axios.post('http://134.209.164.157:5001/object', req.body).then(
      response => {
        ExamLogModel.addExamLogs(req.body, response.data)
          .then(data => {
			
            ExamLogModel.getLiveExamLogById(req.body).then(result=>{
              io.emit('newExamLogs', result)
              return res.json({ data: response.data })
            }).catch(err=>{
              return res.json({ data: response.data })
            })
          })
          .catch(err => {
            return res.json({ data: response.data })
          })
      },
      error => {
        console.log(error)
        return res.json({ status: false, data: error })
      }
    )
  } catch (error) {
    console.log('catch --y', error)
    return res.json({ status: false, data: null })
  }
}

/**
 * fetching both audio and object detection response based onfuserId and examId
 */
exports.streamStatusData = (req, res) => {
  try {
    // axios.post('http://134.122.20.158:5002/status', req.body).then(
      axios.post('http://134.209.164.157:5002/status', req.body).then(
      response => {
       
        // Todo push data to socket
        var logsArray = (response.data.message).split(',')
        var filteredLogs = logsArray.filter(function (el) {
          if (el !== null || el !== '') return el.replace(/[']/g, '')
        })

        
        io.emit('liveExamLogs',{data : req.body,logs :filteredLogs})
        ExamLogModel.addExamLogs(req.body, response.data.message)
          .then(data => {
            return res.json({ data: response.data })
          })
          .catch(err => {
            return res.json({ data: response.data })
          })
      },
      error => {
        console.log(error)
        return res.json({ status: false, data: error })
      }
    )
  } catch (error) {
    console.log('catch --y', error)
    return res.json({ status: false, data: null })
  }
}

/**
 * process killing when user is going offline with streams
 */

exports.killStreamProcess = (req, res) => {
  try {
    console.log("re.body==", req.body);
    // ExamLogModel.updateExamLogs(
    //   { examId: req.body.examId, userId: req.body.userId },
    //   { status: 'dropped' }
    // )
    //   .then(result => {
    //     io.emit('dropExamLogs', { examId: req.body.examId, userId: req.body.userId })
    //     return res.json({ data: result })
    //   }).catch(err => {
    //     return res.json({ data: [] })
    //   })
    axios.post('http://134.209.164.157:5001/kill', req.body).then(
      response => {
        ExamLogModel.updateExamLogs(
          { examId: req.body.examId, userId: req.body.userId },
          { status: req.body.status }
        )
          .then(result => {
            io.emit('dropExamLogs', { examId: req.body.examId, userId: req.body.userId })
            return res.json({ data: result })
          })
          .catch(err => {
            return res.json({ data: response.data })
          })
      },
      error => {
        return res.json({ status: false, data: error })
      }
    )
  } catch (error) {
    return res.json({ status: false, data: null })
  }
}

exports.getById = (req, res) => {
  UserChatModel.findById(req.body.roomId)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'Chatlist details fetched',
        data: result
      })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

exports.recordVideo = (req, res) => {
	let camId = req.body.camId;
	let examSchId = req.body.examSchId;
	var dir = `${__basedir}/public/`;
	if (!fs.existsSync(dir)){
		fs.mkdirSync(dir, 0777);
	}
	var cmd = 'ffmpeg'
	var args = [
	  '-y',
	  '-i',
	  `${camId}`,
	  '-c:a',
	  'aac',
	  '-strict',
	  '-2',
	  '-b:a',
	  '128k',
	  '-c:v',
	  'libx264',
	  `${__basedir}/public/${examSchId}.mp4`
	];
	var proc = spawn(cmd, args);
	proc.stdout.on('data', function(data) {
		console.log("from cmd terminal", data);
	});

	proc.stderr.setEncoding("utf8")
	proc.stderr.on('data', function(data) {
		console.log("on stderr while recording error ", data);
	});

	proc.on('error', function (e) {
      console.log('recording process error' + e)
	  proc.stdin.end()
      proc.kill('SIGINT')
      
    })
    proc.on('exit', function (e) {
      console.log('recording process exit' + e)
	   proc.stdin.end()
       proc.kill('SIGINT')
    })
	
	proc.on('close', function() {
		console.log("", 'finished');
	});
	
	return res.json({ data: proc })
}


exports.captureFromUser = (req, res) => {
  try {
        // Todo push data to socket
        var logsArray = (req.body.message).split(',');
        var filteredLogs = logsArray.filter(function (el) {
          if (el !== null || el !== '') return el.replace(/[']/g, '')
        })

        
        io.emit('liveExamLogs',{data : req.body,logs :filteredLogs})
        ExamLogModel.addExamLogs(req.body, req.body.message)
		  .then(data => {
			return res.json({ data: true })
		  })
		  .catch(err => {
			return res.json({ data: false })
		  })
  } catch (error) {
    console.log('catch --y', error)
    return res.json({ status: false, data: null })
  }
}
