const authModel = require('./../models/auth')

exports.authenticate = (req, res, next) => {
  authModel.authenticate(req.body)
    .then(user => user ? res.json(user) : res.status(400).json({ message: 'Username or password is incorrect' }))
    .catch(err => next(err))
}
exports.getAll = (req, res, next) => {
  console.log(req.headers)
  authModel.getAll()
    .then(users => res.json(users))
    .catch(err => next(err))
}
