const AvailModel = require('./../models/availibility')
const helper = require('../helpers/helper')

exports.insert = (req, res) => {
  const availbility = new AvailModel.Availibility(req.body)

  availbility
    .save()
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'Availibility successfully added',
        data: result
      })
    })
    .catch(err => {
      console.log(err)
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

const validateList = page => {
  if (!page || page === '') {
    return { success: false, message: 'Page number is required', error_code: 2 }
  } else {
    return { success: true }
  }
}

exports.list = (req, res) => {
  console.log('req.query.page==', req.query.page)
  const validate = validateList(req.query.page)
  if (!validate.success) {
    res.status(400).send(validate)
  }
  const limit = 10
  let page = 0
  if (req.query) {
    if (req.query.page) {
      req.query.page = parseInt(req.query.page)
      page = Number.isInteger(req.query.page) ? req.query.page : 0
    }
  }
  AvailModel.list(limit, page)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'Availibility list fetched successfully',
        data: result
      })
    })
    .catch(err => {
      console.log(err)
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

exports.patchById = (req, res) => {
  AvailModel.patchById(req.params.id, req.body)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'Availibility details updated',
        data: result
      })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

exports.removeById = (req, res) => {
  AvailModel.removeById(req.params.id, req.params.status)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'Availibility successfully deleted',
        data: result
      })
    })
    .catch(err => {
      console.log(err)
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}
