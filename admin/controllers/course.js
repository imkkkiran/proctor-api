const helper = require('../helpers/helper')
const CourseModel = require('../models/course')
exports.insert = (req, res) => {
  let { name, createdBy, rulesIds, description,content} = req.body
  const courseId = (name.substring(0, 3)).toUpperCase() + new Date().getTime()
  const obj = { name, createdBy, rulesIds, courseId, description, content }

  const Course = new CourseModel.Course(obj)
  Course.save()
    .then(result => {
      helper.sendResponse(res, { success: true, message: 'Course added successfully', data: result })
    }).catch((err) => {
      console.log(err)
      helper.sendResponse(res, { success: false, message: err.toString(), error_code: 1 })
    })
}
exports.list = (req, res) => {
  CourseModel.list(req.query.page, req.query.fields).then(result => {
    helper.sendResponse(res, { success: true, message: 'Course list  fetched successfully', data: result })
  }).catch((err) => {
    helper.sendResponse(res, { success: false, message: err.toString(), error_code: 1 })
  })
}

exports.removeById = (req, res) => {
  CourseModel.removeById(req.params.courseid, req.params.status).then(result => {
    helper.sendResponse(res, { success: true, message: 'course successfully deleted', data: result })
  }).catch(err => {
    console.log(err)
    helper.sendResponse(res, { success: false, message: err.toString(), error_code: 1 })
  })
}

exports.getById = (req, res) => {
  CourseModel.findById(req.params.courseid).then(result => {
    helper.sendResponse(res, { success: true, message: 'Course details fetched', data: result })
  }).catch((err) => {
    helper.sendResponse(res, { success: false, message: err.toString(), error_code: 1 })
  })
}

exports.patchById = (req, res) => {
  CourseModel.patchCourse(req.params.courseid, req.body).then(result => {
    helper.sendResponse(res, { success: true, message: 'Course details updated', data: result })
  }).catch((err) => {
    helper.sendResponse(res, { success: false, message: err.toString(), error_code: 1 })
  })
}

// exports.update = (req, res) => {
//   let findCriteria = req.body.find
//   let updateCriteria[$set] = req.body.update
//   CourseModel.update(findCriteria,updateCriteria).then(result => {
//     helper.sendResponse(res, { success: true, message: 'Course details fetched', data: result })
//   }).catch((err) => {
//     helper.sendResponse(res, { success: false, message: err.toString(), error_code: 1 })
//   })
// }