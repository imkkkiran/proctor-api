const helper = require('../helpers/helper')
const CourseModel = require('../models/course').Course
const ExamModel = require('../models/exam').Exam
const ExamScheduleModel = require('../models/examSchedule').ExamSchedule
const UserModel = require('../models/user').User
const RuleModel = require('../models/rule').Rule

exports.getCount = async (req, res) => {
  const userId = req.params.userId
  try {
    let result = {}
    if (userId) {
      result = {
        users: 0,
        courses: await CourseModel.count({ status: 'active'}),
        exams: await ExamModel.count({ status: 'active', userId: userId }),
        examScheduled: await ExamScheduleModel.count({
          userId: userId
        }),
        examPending: await ExamScheduleModel.count({
          status: { $in: [ 'pending', 'active' ] },
          userId: userId
        }),
        examDropped: await ExamScheduleModel.count({
          status: 'dropped',
          userId: userId
        }),
        examStarted: await ExamScheduleModel.count({
          status: 'started',
          userId: userId
        }),
        examCompleted: await ExamScheduleModel.count({
          status: 'completed',
          userId: userId
        })
      }
    } else {
      result = {
        users: await UserModel.count({ status: 'active' }),
        rules: await RuleModel.count({ status: 'active' }),
        courses: await CourseModel.count({ status: 'active' }),
        exams: await ExamModel.count({ status: 'active' }),
        examScheduled: await ExamScheduleModel.count(),
        examDropped: await ExamScheduleModel.count({ status: 'dropped' }),
        examStarted: await ExamScheduleModel.count({ status: { $in: [ 'pending', 'active' ] }}),
        examCompleted: await ExamScheduleModel.count({ status: 'completed' })
      }
    }
    helper.sendResponse(res, {
      success: true,
      message: 'counts fetched',
      data: result
    })
  } catch (err) {
    helper.sendResponse(res, {
      success: false,
      message: err.toString(),
      error_code: 1
    })
  }
}

exports.getUserCount = async (req, res) => {
  try {
    let result = {
      courses: await CourseModel.count({ status: 'active' }),
      examScheduled: await ExamScheduleModel.count({ status: 'active',userId:req.body.userId }),
      examDropped: await ExamLogsModel.count({ status: 'dropped',userId:req.body.userId }),
      examStarted: await ExamLogsModel.count({ status: 'started',userId:req.body.userId }),
      examCompleted: await ExamLogsModel.count({ status: 'completed',userId:req.body.userId })
    }
    helper.sendResponse(res, {
      success: true,
      message: 'counts fetched',
      data: result
    })
  } catch (err) {
    helper.sendResponse(res, {
      success: false,
      message: err.toString(),
      error_code: 1
    })
  }
}
