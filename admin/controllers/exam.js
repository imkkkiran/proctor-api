const ExamModel = require('../models/exam')
const helper = require('../helpers/helper')

const ValidateExam = data => {
  if (!data.name || data.name === '') {
    return { success: false, message: 'Exam title is required', error_code: 1 }
  } else if (data.name.length < 3 || data.name.length > 50) {
    return {
      success: false,
      message: 'Exam title should have length between 3 - 50 characters',
      error_code: 2
    }
  } else if (!data.courseId || data.courseId === '') {
    return { success: false, message: 'Select course for exam', error_code: 3 }
  } else if (
    !data.studentLimit ||
    data.studentLimit === '' ||
    data.studentLimit <= 0
  ) {
    return {
      success: false,
      message: 'Enter maximum student count that can give exam',
      error_code: 4
    }
  } else if (!data.price || data.price === '' || data.price <= 0) {
    return {
      success: false,
      message: 'Please add valid price for exam',
      error_code: 5
    }
  }
  // else if (!data.examDate || data.examDate === '')
  // { return { success: false, message: 'Please Select examDate', error_code: 9 } }
  // else if (!data.timeSlot || data.timeSlot === '')
  // { return { success: false, message: 'Please Select timeslot', error_code: 6 } }
  //
  else if (!data.createdBy || data.createdBy === '') {
    return {
      success: false,
      message: 'createdBy field is required',
      error_code: 8
    }
  } else {
    return { success: true }
  }
}

exports.insert = (req, res) => {
  const ExamModel = require('../models/exam').Exam
  const examBody = {
    name: req.body.name,
    courseId: req.body.courseId,
    studentLimit: req.body.studentLimit,
    price: req.body.price,
    examDuration: req.body.examDuration,
    // timeSlot: req.body.timeSlot,
    // examDate: req.body.examDate,
    examFile: req.body.examFile,
    createdBy: req.body.createdBy
  }

  const validate = ValidateExam(examBody)

  if (validate.success) {
    const Exam = new ExamModel(examBody)
    Exam.save()
      .then(result => {
        helper.sendResponse(res, {
          success: true,
          message: 'Exam successfully added',
          data: result
        })
      })
      .catch(err => {
        console.log(err)
        helper.sendResponse(res, {
          success: false,
          message: err.toString(),
          error_code: 1
        })
      })
  } else {
    console.log(validate)
    helper.sendResponse(res, validate)
  }
}

exports.list = (req, res) => {
  ExamModel.list(req.query.page, req.query.fields)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'Exam list  fetched successfully',
        data: result
      })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

exports.usersCourseWithExam = (req, res) => {
  ExamModel.usersCourseAndExam(req.query.page, req.query.fields)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'Success',
        data: result
      })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}
exports.usersCourseExam = (req, res) => {
  ExamModel.usersCourseExam(req.query.page, req.query.fields)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'Success',
        data: result
      })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

exports.removeById = (req, res) => {
  ExamModel.removeById(req.params.examid, req.params.status)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'Exam successfully deleted',
        data: result
      })
    })
    .catch(err => {
      console.log(err)
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

exports.getById = (req, res) => {
  ExamModel.findById(req.params.examid)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'Exam details fetched',
        data: result
      })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

exports.patchById = (req, res) => {
  ExamModel.patchExam(req.params.examid, req.body).then(result => {
    helper.sendResponse(res, { success: true, message: 'Exam details updated', data: result })
  }).catch((err) => {
    helper.sendResponse(res, { success: false, message: err.toString(), error_code: 1 })
  })
}