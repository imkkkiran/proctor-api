const ExamLogsModel = require('../models/examLogs')
const helper = require('../helpers/helper')

exports.getLiveExamLogs = (req, res) => {
  ExamLogsModel.getLiveExamLogs(req.query.page)
    .then((result) => {
      helper.sendResponse(res, {
        success: true,
        message: 'Live Exam logs fetched',
        data: result
      })
    })
    .catch((err) => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

exports.updateExamLogStatus = (req, res) => {
  ExamLogsModel.updateExamLogs(
    { examId: req.body.examId, userId: req.body.userId },
    { status: req.body.status }
  ).then((result) => {
    helper.sendResponse(res, {
      success: true,
      message: 'Exam Status updated.',
      data: result
    })
  })
  .catch((err) => {
    helper.sendResponse(res, {
      success: false,
      message: err.toString(),
      error_code: 1
    })
  })
}
