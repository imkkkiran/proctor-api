const ExamScheduleModel = require('../models/examSchedule')
const ExamModel = require('../models/exam')
const helper = require('../helpers/helper')
const moment = require('moment')

const ValidateScheduledExam = data => {
  if (!data.userId || data.userId === '') {
    return { success: false, message: 'User Id is required', error_code: 1 }
  } else if (!data.examId || data.examId == '') {
    return { success: false, message: 'Exam Id is required', error_code: 2 }
  } else if (!data.createdBy || data.createdBy === '') {
    return {
      success: false,
      message: 'createdBy field is required',
      error_code: 5
    }
  } else {
    return { success: true }
  }
}

async function isStudentLimitExaust (examid) {
  try {
    const examResult = await ExamModel.findById(examid)
    const schedulesExamCount = await ExamScheduleModel.getScheduledExamCount(
      examid
    )

    //console.log(examResult, schedulesExamCount)
    if (examResult.studentLimit > schedulesExamCount) {
      return { status: true }
    } else {
      return {
        status: false,
        message: 'You can not schedule this exam, as exam limit has exceeded.',
        error_code: 6
      }
    }
  } catch (err) {
    console.log(err)
    return { status: false, message: err.toString(), error_code: 7 }
  }
}

async function canScheduleExam (examid) {
  try {
    const examResult = await ExamModel.findById(examid)
    const today = new Date()

    const a = moment(today, 'YYYY-MM-DD')
    const b = moment(examResult.examDate, 'YYYY-MM-DD')
    const diffDays = b.diff(a, 'days')
    //  console.log(diffDays, today, ' | ', examResult.examDate)
    //diffDays >= 1 for if block below
    if (true) {
      return { status: true }
    } else {
      return {
        status: false,
        message: 'You can not schedule exam now',
        error_code: 8
      }
    }
  } catch (err) {
    return { status: false, message: err.toString(), error_code: 9 }
  }
}

async function IsSlotAvailable (examid, selectedSlot) {
  try {
    const examResult = await ExamModel.findById(examid)
    const slotsAvailable = examResult.timeSlot
    const isExist = slotsAvailable.includes(selectedSlot)

    // console.log(slotsAvailable, selectedSlot, ' | ', isExist)
    if (isExist) {
      return { status: true }
    } else {
      return {
        status: false,
        message: 'Selected slot does not exist',
        error_code: 10
      }
    }
  } catch (err) {
    return { status: false, message: err.toString(), error_code: 11 }
  }
}

async function hasScheduledAlready (examId, userId) {
  try {
    const scheduleCount = await ExamScheduleModel.hasScheduledAlready(
      examId,
      userId
    )
    if (!scheduleCount) {
      return { status: true }
    } else {
      return {
        status: false,
        message: 'Exam already scheduled',
        error_code: 12
      }
    }
  } catch (err) {
    return { status: false, message: err.toString(), error_code: 13 }
  }
}

/**
 * Method : Schedule
 * params : userId , examId  , examDate  , timeSlot
 * Validations :
 * Allowed student limit exaust or not
 * Exam has been scheduled already by user/student
 * Can exam be scheduled ?  [Exam can be scheduled 24 hrs before]
 * timeslot selected by student is available or not
 */
exports.schedule = async (req, res) => {
  const examBody = {
    userId: req.body.userId,
    examId: req.body.examId,
    examDate: req.body.examDate,
    timeSlot: req.body.timeSlot,
    createdBy: req.body.userId
  }
  const validate = ValidateScheduledExam(examBody)

  if (validate.success) {
    isStudentLimitExaust(examBody.examId)
      .then(isExaust => {
        if (!isExaust.status) {
          throw isExaust
        } else {
          return hasScheduledAlready(examBody.examId, examBody.userId)
        }
      })
      .then(hasScheduled => {
        // console.log('hasScheduled', hasScheduled)
        if (!hasScheduled.status) {
          throw hasScheduled
        } else {
          const Exam = new ExamScheduleModel.ExamSchedule(examBody)
          return Exam.save()
          //return canScheduleExam(examBody.examId)
        }
      })
      // .then(canSchedule => {
      //   // console.log('canSchedule', canSchedule)
      //   if (!canSchedule.status) {
      //     throw canSchedule
      //   } else {
      //     return IsSlotAvailable(examBody.examId, examBody.timeSlot)
      //   }
      // })
      // .then(isSlotExist => {
      //   if (!isSlotExist.status) {
      //     throw isSlotExist
      //   } else {
      //     const Exam = new ExamScheduleModel.ExamSchedule(examBody)
      //     return Exam.save()
      //   }
      // })
      .then(result => {
        return helper.sendResponse(res, {
          success: true,
          message: 'Exam scheduled successfully',
          data: result
        })
      })
      .catch(err => {
        console.log(err)
        return helper.sendResponse(res, {
          success: false,
          message: err.message,
          error_code: err.error_code
        })
      })
  } else {
    return helper.sendResponse(res, validate)
  }
}

exports.getById = (req, res) => {
  ExamScheduleModel.findById(req.params.examscheuleid)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'Exam Schedules details fetched',
        data: result
      })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

exports.removeById = (req, res) => {
  ExamScheduleModel.removeById(req.params.examscheuleid)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'Exam scheduled successfully deleted',
        data: result
      })
    })
    .catch(err => {
      console.log(err)
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

exports.getByUserId = (req, res) => {
  console.log('req.params.examscheuleid-', req.params.examscheuleid)
  ExamScheduleModel.getByUserId(req.params.examscheuleid)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'Exam Schedules details fetched',
        data: result
      })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

exports.patchById = (req, res) => {
  const updateCriteria = req.body.fieldsToUpdate
  ExamScheduleModel.updateScheduledExams(
    { _id: req.params.examscheuleid },
    { $set: updateCriteria },
    true
  )
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'Exam details updated',
        data: result
      })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

exports.getLiveExamLogs = (req, res) => {
  ExamScheduleModel.getLiveExamLogs(req.query.page)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'Live Exam logs fetched',
        data: result
      })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

exports.list = (req, res) => {
  if (req.body.status === 'all')
    var filter = { status: { $ne: 'deleted' } }
  else 
    var filter = { status: { $eq: req.body.status } }

  ExamScheduleModel.list(req.body.page, req.body.fields, filter)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'Exam list  fetched successfully',
        data: result
      })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}



exports.updateExamLogStatus = (req, res) => {
  ExamScheduleModel.updateExamLogs(
    { examId: req.body.examId, userId: req.body.userId },
    { status: req.body.status }
  ).then((result) => {
    helper.sendResponse(res, {
      success: true,
      message: 'Exam Status updated.',
      data: result
    })
  })
  .catch((err) => {
    helper.sendResponse(res, {
      success: false,
      message: err.toString(),
      error_code: 1
    })
  })
}

exports.chatlist = (req, res) => {
  ExamScheduleModel.chatList()
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'User chat list fetched successfully',
        data: result
      })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}
