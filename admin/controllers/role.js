const RoleModel = require('./../models/role')
const helper = require('../helpers/helper')


exports.insert = (req, res) => {

  const role = new RoleModel.Role(req.body)
  role.save()
    .then(result => {
      helper.sendResponse(res, { success: true, message: 'Role successfully added', data: result })
    }).catch((err) => {
      console.log(err)
      helper.sendResponse(res, { success: false, message: err.toString(), error_code: 1 })
    })
}

const validateList = (page) => {
  if (!page || page === '') { return { success: false, message: 'Page number is required', error_code: 2 } } else { return { success: true } }
}

exports.list = (req, res) => {
  const validate = validateList(req.query.page)
  if (!validate.success) { res.status(400).send(validate) }
  const limit = 10
  let page = 0
  if (req.query) {
    if (req.query.page) {
      req.query.page = parseInt(req.query.page)
      page = Number.isInteger(req.query.page) ? req.query.page : 0
    }
  }
  RoleModel.list(limit, page).then(result => {
    helper.sendResponse(res, { success: true, message:  'Roles list fetched successfully', data: result })
  }).catch((err) => {
    console.log(err)
    helper.sendResponse(res, { success: false, message: err.toString(), error_code: 1 })
  })
}

exports.patchById = (req, res) => {
  RoleModel.patchRole(req.params.roleid, req.body).then(result => {
    helper.sendResponse(res, { success: true, message: 'Role details updated', data: result })
  }).catch((err) => {
    helper.sendResponse(res, { success: false, message: err.toString(), error_code: 1 })
  })
}

exports.removeById = (req, res) => {
  RoleModel.removeById(req.params.roleid, req.params.status).then(result => {
    helper.sendResponse(res, { success: true, message: 'Role successfully deleted', data: result })
  }).catch(err => {
    console.log(err)
    helper.sendResponse(res, { success: false, message: err.toString(), error_code: 1 })
  })
}
