const RuleModel = require('../models/rule')
const helper = require('../helpers/helper')

exports.insert = (req, res) => {
  console.log('ruleDoc',req.body)
  let ruleDoc = {
    ruleTitle: req.body.ruleName,
    subRuleTitle: req.body.subrule,
    createdBy: req.body.createdBy,
    updatedBy: req.body.updatedBy,
    description: req.body.description ? req.body.description : ''
  }
  let ruleModel = require('../models/rule').Rule
  
  ruleModel.create(ruleDoc).then(result => {
    helper.sendResponse(res, { success: true, message: 'Rule successfully added', data: result })
  }).catch(err => {
    
    if(err.toString().includes('ruleTitle_1 dup key')  )
    helper.sendResponse(res, { success: false, message:'Rule already exist', error_code: 2 })
    else 
    helper.sendResponse(res, { success: false, message: err.toString(), error_code: 1 })
  })
}

const listValidation = (page) => {
  if (!page || page === '') { return { success: false, message: 'Page number is required', error_code: 2 } } else { return { success: true } }
}
exports.list = (req, res) => {
  const validate = listValidation(req.query.page)
  if (validate.success) {
    const limit = 10
    let page = 0
    if (req.query) {
      if (req.query.page) {
        req.query.page = parseInt(req.query.page)
        page = Number.isInteger(req.query.page) ? req.query.page : 0
      }
    }
    RuleModel.list(limit, page).then(result => {
      helper.sendResponse(res, { success: true, message: 'Rules list fetched successfully', data: result })
    }).catch((err) => {
      helper.sendResponse(res, { success: false, message: err.toString(), error_code: 1 })
    })
  } else {
    helper.sendResponse(res, validate)
  }
}

exports.listForModule = (req, res) => {
  const validate = listValidation(req.query.page)
  if (validate.success) {
    const limit = 10
    let page = 0
    if (req.query) {
      if (req.query.page) {
        req.query.page = parseInt(req.query.page)
        page = Number.isInteger(req.query.page) ? req.query.page : 0
      }
    }
    RuleModel.listForModule(limit, page).then(result => {
      helper.sendResponse(res, { success: true, message: 'Rules list fetched successfully', data: result })
    }).catch((err) => {
      helper.sendResponse(res, { success: false, message: err.toString(), error_code: 1 })
    })
  } else {
    helper.sendResponse(res, validate)
  }
}
exports.removeById = (req, res) => {
  RuleModel.removeById(req.params.ruleid, req.params.status).then(result => {
    helper.sendResponse(res, { success: true, message: 'Rule successfully deleted', data: result })
  }).catch(err => {
    console.log(err)
    helper.sendResponse(res, { success: false, message: err.toString(), error_code: 1 })
  })
}

exports.getById = (req, res) => {
  RuleModel.findById(req.params.ruleid).then(result => {
    helper.sendResponse(res, { success: true, message: 'Rule details fetched', data: result })
  }).catch((err) => {
    helper.sendResponse(res, { success: false, message: err.toString(), error_code: 1 })
  })
}

exports.patchById = (req, res) => {
  RuleModel.patchRule(req.params.ruleid, req.body).then(result => {
    helper.sendResponse(res, { success: true, message: 'Rule details updated', data: result })
  }).catch((err) => {
    helper.sendResponse(res, { success: false, message: err.toString(), error_code: 1 })
  })
}
