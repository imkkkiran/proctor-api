const ExamScheduleModel = require('../models/examSchedule')

exports.updateUserVerifiedStatus = data => {
  const examScheduledId = data.examScheduledId
  const userId = data.userId
  const userVerifiedStatus = data.userVerifiedStatus
  const findCriteria = { _id: examScheduledId }
  const updateCriteria = { $set: { userVerifiedStatus: userVerifiedStatus } }
  ExamScheduleModel.updateScheduledExams(findCriteria, updateCriteria, true)
    .then(result => {
      return { success: true, data: result }
    })
    .catch(err => {
      return { success: false, message: err.toString() }
    })
}

const io = require('socket.io')(server)
io.use((socket, next) => {
  const token = socket.handshake.query.token
  // verify token
  jsonwebtoken.verify(token, jsonConfig.secret, (err, decoded) => {
    if (err) {
      console.log(err.toString())
      socket.emit('connect_error', err.toString())
      return next({ error: 'Authentication failed' })
    }
    // set the user’s mongodb _id to the socket for future use
    socket._id = decoded.sub
    console.log('decoded', decoded)
    next()
  })
})

let interval
io.on('connection', function (socket) {
  console.log('New Client is Connected!')
  if (interval) {
    clearInterval(interval)
  }
  interval = setInterval(() => getApiAndEmit(socket), 1000)
  socket.on('disconnect', () => {
    console.log('Client disconnected')
    clearInterval(interval)
  })
  socket.on('VerifyUserResponse', function (data) {
    console.log('Recieced in node server verifyUser Event data', data)
    updateUserVerifiedStatus(data)
  })

  socket.on('sharedScreenResponse', function (data) {
    console.log('Recieced in node server sharedScreenResponse Event data', data)
  })
})

const getApiAndEmit = socket => {
  const response = new Date()
  // Emitting a new message. Will be consumed by the client
  socket.emit('FromAPI', response)
}