const crypto = require('crypto')
const UserModel = require('../models/user')
const helper = require('../helpers/helper')

const env = process.env.NODE_ENV || 'development'
const config = require(`./../../config/${env}.js`)
const FROM_EMAIL = config.FROM_EMAIL
const SERVER_URL = config.SERVER_URL
const mailService = require('./../../config/mail')

const insertValidation = data => {
  // if (!data.username || data.username === '') {
  //   return { success: false, message: 'Username is required', error_code: 1 }
  // } else
  if (!data.email || data.email === '') {
    return { success: false, message: 'Email is required', error_code: 2 }
  } else if (!data.password || data.password === '') {
    return { success: false, message: 'Password is required', error_code: 3 }
  } else if (!data.createdBy || data.createdBy === '') {
    return { success: false, message: 'CreatedBy is required', error_code: 4 }
  } else if (!data.name || data.name === '') {
    return { success: false, message: 'Name is required', error_code: 5 }
  } else if (!data.roleId || data.roleId === '') {
    return { success: false, message: 'RoleId is required', error_code: 6 }
  } else {
    return { success: true }
  }
}

exports.insert = (req, res) => {
  let validate = insertValidation(req.body)
  if (validate.success) {
    const salt = crypto.randomBytes(16).toString('base64')
    const hash = crypto
      .createHmac('sha256', salt)
      .update(req.body.password)
      .digest('base64')
    req.body.salt = salt
    req.body.password = salt + '$' + hash
    const UserModel = require('../models/user').User
    const user = new UserModel(req.body)
    user
      .save()
      .then(result => {
        helper.sendResponse(res, {
          success: true,
          message: 'User successfully Registered',
          data: { id: result._id }
        })
      })
      .catch(err => {
        console.log(err)
        let errMsg = err.toString()

        if (errMsg.includes('createdBy: Cast to ObjectID'))
          errMsg = 'CreatedBy is invalid'
        else if (errMsg.includes('roleId: Cast to ObjectID'))
          errMsg = 'Role is invalid'
        // else if (errMsg.includes('username_1 dup key'))
        //   errMsg = 'Username is not available'
        else if (errMsg.includes('email_1 dup key'))
          errMsg = 'User account with this email already exist'

        helper.sendResponse(res, {
          success: false,
          message: errMsg,
          error_code: 1
        })
      })
  } else {
    helper.sendResponse(res, validate)
  }
}

exports.login = (req, res) => {
  const UserModel = require('../models/user')

  UserModel.login(req.body)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'Successfully logged in',
        data: result.data
      })
    })
    .catch(err => {
      console.log(err)
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

const listValidation = page => {
  if (!page || page === '') {
    return { success: false, message: 'Page number is required', error_code: 2 }
  } else {
    return { success: true }
  }
}
exports.list = (req, res) => {
  const validate = listValidation(req.query.page)
  if (validate.success) {
    const limit = 100
    let page = 0
    if (req.query) {
      if (req.query.page) {
        req.query.page = parseInt(req.query.page)
        page = Number.isInteger(req.query.page) ? req.query.page : 0
      }
    }
    const UserModel = require('../models/user')
    UserModel.list(limit, page, req.query.fields)
      .then(result => {
        helper.sendResponse(res, {
          success: true,
          message: 'Users fetched successfully',
          data: result
        })
      })
      .catch(err => {
        helper.sendResponse(res, {
          success: false,
          message: err.toString(),
          error_code: 1
        })
      })
  } else {
    helper.sendResponse(res, validate)
  }
}

exports.getById = (req, res) => {
  UserModel.findById(req.params.userid)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'User details fetched',
        data: result
      })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

exports.patchById = (req, res) => {
  if (req.body.password) {
    const salt = crypto.randomBytes(16).toString('base64')
    const hash = crypto
      .createHmac('sha512', salt)
      .update(req.body.password)
      .digest('base64')
    req.body.password = salt + '$' + hash
  }
  UserModel.patchUser(req.params.userid, req.body)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'User details updated',
        data: result
      })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

exports.removeById = (req, res) => {
  UserModel.removeById(req.params.userid, req.params.status)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'User deleted successfully',
        data: result
      })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

exports.changePassword = (req, res) => {
  const userId = req.body.userId
  const oldPassword = req.body.oldPassword
  const newPassword = req.body.newPassword
  UserModel.verifyPassword({ oldPassword, newPassword, userId })
    .then(verified => {
      const salt = crypto.randomBytes(16).toString('base64')
      const hash = crypto
        .createHmac('sha256', salt)
        .update(newPassword)
        .digest('base64')
      console.log('newPassword', newPassword)
      const password = salt + '$' + hash
      UserModel.patchUser(userId, { password, salt })
        .then(result => {
          helper.sendResponse(res, {
            success: true,
            message: 'Password updated successfully',
            data: {}
          })
        })
        .catch(err => {
          helper.sendResponse(res, {
            success: false,
            message: err.toString(),
            error_code: 2
          })
        })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

exports.recover = (req, res) => {
  UserModel.User.findOne({ email: req.body.email })
    .then(user => {
      if (!user) {
        helper.sendResponse(res, {
          success: false,
          message: 'User not found',
          error_code: 1
        })
      }
      //Generate and set password reset token
      let userObj = UserModel.generatePasswordReset()

      // Save the updated user object
      UserModel.patchUser(user._id, userObj)
        .then(user => {
          // send email
          const link = SERVER_URL + 'verify-reset-pass/?token=' + user.resetPasswordToken
          const mailOptions = {
            to: user.email,
            from: FROM_EMAIL,
            subject: 'Password change request',
            html: `Hi ${user.username} \n 
              Please click on the following link ${link} to reset your password. \n\n 
              If you did not request this, please ignore this email and your password will remain unchanged.\n`
          }
          console.log(mailOptions)
          mailService
            .sendMail(mailOptions)
            .then(result => {
              helper.sendResponse(res, {
                success: true,
                message: 'A reset email has been sent to ' + user.email + '.',
                data: result
              })
            })
            .catch(error => {
              console.log('ERROR - Send Mail ', error)
              helper.sendResponse(res, {
                success: false,
                message: 'Please try again later',
                error_code: 3
              })
            })
        })
        .catch(err => {
          helper.sendResponse(res, {
            success: false,
            message: err.message,
            error_code: 2
          })
        })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.message,
        error_code: 2
      })
    })
}

// @route POST api/auth/reset
// @desc Reset Password - Validate password reset token and shows the password reset view
// @access Public
exports.reset = (req, res) => {
  UserModel.User.findOne({
    resetPasswordToken: req.body.token,
    resetPasswordExpires: { $gt: Date.now() }
  })
    .then(user => {
      if (!user) {
        helper.sendResponse(res, {
          success: false,
          message: 'Password reset token is invalid or has expire',
          error_code: 1
        })
      } else {
        helper.sendResponse(res, {
          success: true,
          message: 'Forgot password link verified',
          data: {}
        })
      }
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 2
      })
    })
}

// @route POST api/auth/reset
// @desc Reset Password
// @access Public
exports.resetPassword = (req, res) => {
  UserModel.User.findOne({
    resetPasswordToken: req.body.token,
    resetPasswordExpires: { $gt: Date.now() }
  }).then(user => {
    if (!user) {
      helper.sendResponse(res, {
        success: false,
        message: 'Password reset token is invalid or has expired.',
        error_code: 1
      })
    }
    //Set the new password
	const salt = crypto.randomBytes(16).toString('base64')
    const hash = crypto
      .createHmac('sha256', salt)
      .update(req.body.password)
      .digest('base64')
    user.password = salt + '$' + hash
	user.salt = salt
    user.resetPasswordToken = undefined
    user.resetPasswordExpires = undefined
	console.log("User Data", user);
    // Save
    user.save(err => {
      if (err) {
        helper.sendResponse(res, {
          success: false,
          message: err.message,
          error_code: 2
        })
      }

      // send email
      const mailOptions = {
        to: user.email,
        from: FROM_EMAIL,
        subject: 'Your password has been changed',
        text: `Hi ${user.username} \n 
                  This is a confirmation that the password for your account ${user.email} has just been changed.\n`
      }

      mailService
        .sendMail(mailOptions)
        .then(result => {
          helper.sendResponse(res, {
            success: true,
            message: 'Your password has been updated.',
            data: result
          })
        })
        .catch(error => {
          console.log('ERROR - Send Mail ', error)
          helper.sendResponse(res, {
            success: false,
            message: 'Please try again later',
            error_code: 3
          })
        })
    })
  })
}

exports.dashboardCounts = (req, res) => {
  // let usersCount = UserModel.User.count({
  //   status: {$ne:'deleted'}
  // });
  // const CourseModel = require('../models/course');
  // let courseCount = CourseModel.Course.count({
  //   status: {$ne:'deleted'}
  // })
  // res.send({
  //   success: true,
  //   message: 'Success',
  //   data: {usersCount}
  // });
}
exports.userDetails = (req, res) => {
  UserModel.userDetails(req.params.userid)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'User details fetched successfully',
        data: result
      })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}

exports.updateFav = (req, res) => {
  UserModel.updateFav(req.body.userId, req.body.favStatus)
    .then(result => {
      helper.sendResponse(res, {
        success: true,
        message: 'Updated successfully.',
        data: result
      })
    })
    .catch(err => {
      helper.sendResponse(res, {
        success: false,
        message: err.toString(),
        error_code: 1
      })
    })
}