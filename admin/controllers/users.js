const crypto = require('crypto')
const UserModel = require('../models/users')

exports.insert = (req, res) => {
  const salt = crypto.randomBytes(16).toString('base64')
  const hash = crypto.createHmac('sha256', salt).update(req.body.password).digest('base64')
  req.body.salt = salt
  req.body.password = salt + '$' + hash
  console.log(req.body)
  const user = new UserModel(req.body)
  user.save()
    .then(result => {
      res.status(200).send({ success: true, message: 'Successfully Registered', data: { id: result._id } })
    }).catch((err) => {
      console.log(err)
      res.status(400).send({ success: false, message: err.toString(), error_code: 1 })
    })
}



exports.login = (req, res) => {
  const email = req.body.email
  const password = req.body.password
  UserModel.login(req.body).then((result) => {
    res.status(200).send({ success: true, message: 'Successfully logged in', data: result })
  }).catch((err) => {
    res.status(400).send(err)
  })

}
exports.getById = (req, res) => {
    UserModel.findById(req.params.userId).then(result => {
        res.status(200).send(result)
    })
}

exports.patchById = (req, res) => {
    if (req.body.password) {
        let salt = crypto.randomBytes(16).toString('base64')
        let hash = crypto
            .createHmac('sha512', salt)
            .update(req.body.password)
            .digest('base64')
        req.body.password = salt + '$' + hash
    }
    UserModel.patchUser(req.params.userId, req.body).then(result => {
        res.status(204).send({})
    })
}
exports.list = (req, res) => {
    let limit =
        req.query.limit && req.query.limit <= 100 ? parseInt(req.query.limit) : 10
    let page = 0
    if (req.query) {
        if (req.query.page) {
            req.query.page = parseInt(req.query.page)
            page = Number.isInteger(req.query.page) ? req.query.page : 0
        }
    }
    UserModel.list(limit, page).then(result => {
        res.status(200).send(result)
    })
}

exports.removeById = (req, res) => {
    UserModel.removeById(req.params.userId).then(result => {
        res.status(204).send({})
    })
}
