exports.sendResponse = (res, printObject) => {
  if (printObject.success) {
    res.status(200).send({
      success: printObject.success,
      message: printObject.message,
      data: printObject.data
    })
  } else {
    res.status(400).send({
      success: printObject.success,
      message: printObject.message,
      error_code: printObject.error_code
    })
  }
}

// exports.nextSeqVal = (sequenceName,fromCollection)=>
// {
//   const roleModel = require('./../models/role').Role
//   const role = new roleModel(req.body)
// }
