
const multer = require('multer')
const path = require('path')

exports.uploadSingle = (filename, filePath, filesAllowed) => {
  const storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, filePath)
    },
    filename: function (req, file, cb) {
      cb(null, file.originalname)
    }
  })

  const fileFilter = (req, file, cb) => {
    // reject a file
    var filetypes = filesAllowed
    var mimetype = filetypes.test(file.mimetype)

    var extname = filetypes.test(path.extname(
      file.originalname).toLowerCase())

    if (mimetype && extname) { return cb(null, true) } else { return cb('Unsupported file formats', false) }
  }

  const upload = multer({
    storage: storage,
    limits: {
      fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
  })

  return upload.single(filename, function (err, res, next) {
    if (err) { console.log(err) } else { next() }
  })
}
