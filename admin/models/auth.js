const config = require('./../../config/config')
const jwt = require('jsonwebtoken')

// users hardcoded for simplicity, store in a db for production applications
const users = [{ id: 1, username: 'test', password: 'test', firstName: 'Test', lastName: 'User' }]

async function authenticate (body) {
  const { username, password } = body
  const user = users.find(u => u.username === username && u.password === password)
  if (user) {
    const token = jwt.sign({ sub: user.id }, config.secret)
    const { password, ...userWithoutPassword } = user
    return {
      ...userWithoutPassword,
      token
    }
  }
}

async function getAll () {
  return users
}

module.exports = {
  authenticate,
  getAll
}
