const mongoose = require('mongoose')
const { Schema } = mongoose

const availibilitySchema = new Schema({
  examDate: { type: Array, required: 'Exam Date is required' },
  timeslot: { type: Array, required: 'Exam Time is required' },
  status: { type: String, default: 'active', enum: ['active', 'inactive', 'deleted'] },
  createdAt: { type: Date, default: Date.now },
  createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  updatedAt: { type: Date, default: Date.now }
})

availibilitySchema.pre('save', function (next) {
  const now = Date.now()
  this.updatedAt = now
  this.status = 'active'
  // Set a value for createdAt only if it is null
  if (!this.createdAt) {
    this.createdAt = now
  }
  // Call the next function in the pre-save chain
  next()
})

exports.findById = id => {
  return Availibility.findById(id).then(result => {
    result = result.toJSON()
    delete result._id
    delete result.__v
    return result
  })
}

exports.list = (perPage, page) => {
  return new Promise((resolve, reject) => {
    Availibility.find({ status: { $ne: 'deleted' } })
      .populate('createdBy', 'name email username status')
      .limit(perPage)
      .skip(perPage * page)
      .lean()
      .exec(function (err, result) {
        if (err) {
          reject(err)
        } else {
          resolve(result)
        }
      })
  })
}

exports.patchById = (id, data) => {
  return new Promise((resolve, reject) => {
    Availibility.findById(id, function (err, availibility) {
      if (err) reject(err)
      for (const i in data) {
        availibility[i] = data[i]
      }

      availibility.save(function (err, updatedDoc) {
        if (err) return reject(err)
        resolve(updatedDoc)
      })
    })
  })
}

exports.removeById = (id, status = 'deleted') => {
  return new Promise((resolve, reject) => {
    Availibility.updateOne(
      { _id: id },
      { $set: { status: status } },
      (err, result) => {
        if (err) {
          reject(err)
        }
        resolve(result)
      }
    )
  })
}

exports.update = (findCriteria, updateCriteria) => {
  return new Promise((resolve, reject) => {
    console.log(findCriteria, updateCriteria)
    Availibility.update(
      findCriteria,
      updateCriteria,
      { multi: true },
      (err, result) => {
        if (err) {
          reject(err)
        } else {
          resolve(result)
        }
      }
    )
  })
}
const Availibility = mongoose.model('Availibility', availibilitySchema)

exports.Availibility = Availibility
