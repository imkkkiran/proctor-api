const mongoose = require('mongoose')

const courseSchema = mongoose.Schema({
  name: { type: String, unique: true, required: 'course name is required' },
  description: { type: String, required: 'course description is required' },
  content: { type: String, required: 'course content is required' },
  courseId: { type: String },
  status: { type: String, default: 'active', enum: ['active', 'inactive', 'deleted'] },
  createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: 'CreatedBy field is required' },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  rulesIds: [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Rule'
  }]
})

courseSchema.pre('save', function (next) {
  const now = Date.now()
  this.updatedAt = now
  // Set a value for createdAt only if it is null
  if (!this.createdAt) {
    this.createdAt = now
  }
  // Call the next function in the pre-save chain
  next()
})

const Course = mongoose.model('Course', courseSchema)

exports.list = (page, fields) => {
  let perPage = 10
  let projectCriteria = {}
  return new Promise((resolve, reject) => {
    if (fields !== undefined && fields !== '') {
      fieldsArray = fields.split(',')
      fieldsArray.map(field => { projectCriteria[field] = 1 })

    }
    if (page !== undefined && page >= 0) {
      Course.find({ status: { $ne: 'deleted' } }, projectCriteria)
        .populate('createdBy', 'name email username status')
        .populate('rulesIds', 'ruleTitle subRuleTitle status')
        .limit(perPage).skip(perPage * page).sort({ createdAt: -1 }).lean().exec(function (err, result) {
          if (err) {
            reject(err)
          } else {
            resolve(result)
          }
        })
    } else {
      // gives all courses without populate method
      Course.find({ status: { $ne: 'deleted' } }, projectCriteria).sort({ createdAt: -1 }).lean().exec(function (err, result) {
        if (err) {
          reject(err)
        } else {
          resolve(result)
        }
      })
    }
  })
}

exports.removeById = (courseId, status = 'deleted') => {
  return new Promise((resolve, reject) => {
    console.log({ _id: courseId })
    Course.updateOne({ _id: courseId }, { $set: { status: status } }, (err, result) => {
      if (err) {
        reject(err)
      } else {
        resolve(result)
      }
    })
  })
}

exports.findById = (id) => {
  return new Promise((resolve, reject) => {
    Course.findById({ _id: mongoose.Types.ObjectId(id) }, { 'createdBy.salt': 0, 'createdBy.password': 0 })
      .populate('createdBy', 'name email username status')
      .populate('rulesIds', 'ruleTitle subRuleTitle status')
      .lean().exec((err, result) => {
        if (err) {
          reject(err)
        }
        resolve(result)
      })
  })
}
exports.update = (findCriteria, updateCriteria) => {
  return new Promise((resolve, reject) => {
    console.log(findCriteria, updateCriteria)
    Course.update(findCriteria, updateCriteria, (err, result) => {
      if (err) {
        reject(err)
      } else {
        resolve(result)
      }
    })
  })
}
exports.patchCourse = (id, courseData) => {
  return new Promise((resolve, reject) => {
    Course.findById(id, function (err, course) {
      if (err) reject(err)
      for (const i in courseData) {
        course[i] = courseData[i]
      }

      course.save(function (err, updatedCourse) {
        if (err) return reject(err)
        resolve(updatedCourse)
      })
    })
  })
}
exports.Course = Course
