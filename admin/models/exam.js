const mongoose = require('mongoose')

const examSchema = mongoose.Schema({
  name: { type: String, required: 'Name is required' },
  examFile: { type: String, required: 'Exam File path is required' },
  studentLimit: { type: String, required: 'Exam File path is required' },
  examDuration: { type: String, required: 'Exam Duration is required' },
  // examDate: { type: Array, required: 'Exam Date is required' },
  price: { type: String, required: 'Add exam price' },
  priceUnit: { type: String, required: 'Add currency unit', default: 'USD' },
  // timeSlot: { type: Array, required: 'Select time slot for exam' },
  status: {
    type: String,
    default: 'active',
    enum: ['active', 'inactive', 'deleted']
  },
  courseId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Course',
    required: 'Select course of exam'
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: 'createdBy is required'
  },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
})

examSchema.pre('save', function (next) {
  const now = Date.now()
  this.updatedAt = now
  if (!this.createdAt) {
    this.createdAt = now
  }
  next()
})

const Exam = mongoose.model('Exam', examSchema)

exports.findById = id => {
  return new Promise((resolve, reject) => {
    Exam.findById(id, function (err, result) {
      if (err) {
        reject(err)
      }
      resolve(result)
    })
  })
}

exports.list = (page, fields) => {
  let perPage = 10
  let projectCriteria = {}
  return new Promise((resolve, reject) => {
    if (fields !== undefined && fields !== '') {
      fieldsArray = fields.split(',')
      fieldsArray.map(field => {
        projectCriteria[field] = 1
      })
    }

    if (page !== undefined && page >= 0) {
      Exam.find({ status: { $ne: 'deleted' } }, projectCriteria)
        .populate('courseId', 'name status')
        .populate('createdBy', 'name email username status')
        .limit(perPage)
        .skip(perPage * page)
        .sort({ createdAt: -1 })
        .lean()
        .exec(function (err, result) {
          if (err) {
            reject(err)
          } else {
            resolve(result)
          }
        })
    } else {
      // gives all exams without populate method
      Exam.find({ status: { $ne: 'deleted' } }, projectCriteria)
        .sort({ createdAt: -1 })
        .lean()
        .exec(function (err, result) {
          if (err) {
            reject(err)
          } else {
            resolve(result)
          }
        })
    }
  })
}

exports.usersCourseAndExam = (page, fields) => {
  let perPage = 10
  let projectCriteria = {}
  return new Promise((resolve, reject) => {
    if (fields !== undefined && fields !== '') {
      fieldsArray = fields.split(',')
      fieldsArray.map(field => {
        projectCriteria[field] = 1
      })
    }
    if (page !== undefined && page >= 0) {
      Exam.find({ status: { $ne: 'deleted' } }, projectCriteria)
        .populate('courseId', 'name status content')
        .limit(perPage)
        .skip(perPage * page)
        .sort({ createdAt: -1 })
        .lean()
        .exec(function (err, result) {
          if (err) {
            reject(err)
          } else {
            resolve(result)
          }
        })
    } else {
      // gives all courses without populate method
      Exam.find({ status: { $ne: 'deleted' } }, projectCriteria)
        .sort({ createdAt: -1 })
        .lean()
        .exec(function (err, result) {
          if (err) {
            reject(err)
          } else {
            resolve(result)
          }
        })
    }
  })
}
exports.usersCourseExam = (page, fields) => {
  return new Promise((resolve, reject) => {
    Exam.aggregate([
     {$match:{status: { $eq: 'active' }}},
      {
        
        $group: {
          _id: '$courseId',

          exams: { $push: { examId: '$_id', link: '$examFile', name: '$name' } }
        }
      },
      {
        $lookup: {
          from: 'courses',
          localField: '_id',
          foreignField: '_id',
          as: 'course'
        }
      }
    ])
      .exec()
      .then(resolve)
      .catch(reject)
  })
}

exports.removeById = (examId, status = 'deleted') => {
  return new Promise((resolve, reject) => {
    console.log({ _id: examId })
    Exam.updateOne(
      { _id: examId },
      { $set: { status: status } },
      (err, result) => {
        if (err) {
          reject(err)
        } else {
          resolve(result)
        }
      }
    )
  })
}

exports.findById = id => {
  return new Promise((resolve, reject) => {
    Exam.findById({ _id: mongoose.Types.ObjectId(id) })
      .populate('courseId', 'name status')
      .populate('createdBy', 'name email username status')
      .lean()
      .exec((err, result) => {
        if (err) {
          reject(err)
        }
        resolve(result)
      })
  })
}

exports.patchExam = (id, examData) => {
  return new Promise((resolve, reject) => {
    Exam.findById(id, function (err, exam) {
      if (err) reject(err)
      for (const i in examData) {
        exam[i] = examData[i]
      }

      exam.save(function (err, updatedExam) {
        if (err) return reject(err)
        resolve(updatedExam)
      })
    })
  })
}

exports.Exam = Exam
