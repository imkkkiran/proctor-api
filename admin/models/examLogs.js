const mongoose = require('mongoose')

const examLogsSchema = mongoose.Schema({
  examId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Exam'
  },
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User'
  },
  streamUrl: { type: String },
  isExamStarted: { type: Boolean },
  examDuration: { type: String },
  logs: { type: Array },
  status: {
    type: String,
    default: 'started',
    enum: ['started', 'completed', 'dropped']
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: 'CreatedBy field is required'
  },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
})

examLogsSchema.pre('save', function (next) {
  const now = Date.now()
  this.updatedAt = now
  // Set a value for createdAt only if it is null
  if (!this.createdAt) {
    this.createdAt = now
  }
  // Call the next function in the pre-save chain
  next()
})

const ExamLog = mongoose.model('ExamLog', examLogsSchema)

async function addExamLogs (data, logs) {
  return new Promise((resolve, reject) => {
    let updateCriteria = {}
    var logsArray = logs.split(',')
    var filteredLogs = logsArray.filter(function (el) {
      if (el !== null || el !== '') return el.replace(/[']/g, '')
    })

    if (data.camId !== undefined && data.camId !== '') {
      updateCriteria = {
        $set: { streamUrl: data.camId, status: 'started' },
        $push: { logs: filteredLogs }
      }
    } else {
      updateCriteria = {
        $push: { logs: filteredLogs }
      }
    }
    // console.log('data',data,'updateCriteria',updateCriteria)
    ExamLog.update(
      { examId: data.examId, userId: data.userId },
      updateCriteria,
      { upsert: true },
      (err, result) => {
        if (err) {
          return reject(err)
        } else {
          return resolve(result)
        }
      }
    )
  })
}

exports.updateExamLogs = (updateCriteria, updateFields) => {
  return new Promise((resolve, reject) => {
    ExamLog.update(updateCriteria, { $set: updateFields }, (err, result) => {
      if (err) {
        return reject(err)
      } else {
        return resolve(result)
      }
    })
  })
}

exports.getLiveExamLogs = page => {
  let perPage = 10
  return new Promise((resolve, reject) => {
    ExamLog.find(
      { status: 'started' },
      {
        examId: 1,
        logs: 1,
        updatedAt: 1,
        createdBy: 1,
        status: 1,
        streamUrl: 1,
        userId: 1
      }
    )
      .populate('examId', 'name status')
      .populate('userId', 'name email username status userImage')
      .limit(perPage)
      .skip(perPage * page)
      .sort({ createdAt: -1 })
      .lean()
      .exec(function (err, result) {
        if (err) {
          reject(err)
        } else {
          resolve(result)
        }
      })
  })
}
exports.getLiveExamLogById = (data) => {
  return new Promise((resolve, reject) => {
    ExamLog.findOne(
      {examId: data.examId, userId: data.userId, status: 'started' },
      {
        examId: 1,
        logs: 1,
        updatedAt: 1,
        createdBy: 1,
        status: 1,
        streamUrl: 1,
        userId: 1
      }
    )
      .populate('examId', 'name status')
      .populate('userId', 'name email username status')
      .sort({ createdAt: -1 })
      .lean()
      .exec(function (err, result) {
        if (err) {
          reject(err)
        } else {
          resolve(result)
        }
      })
  })
}

exports.addExamLogs = addExamLogs


exports.ExamLog = ExamLog