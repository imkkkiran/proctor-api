const mongoose = require('mongoose')

const examScheduleSchema = mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: 'Student id required'
  },
  examId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Exam',
    required: 'Exam id is required'
  },
  // examDate: { type: Date, required: 'Exam Date is required' },
  // timeSlot: { type: Array, required: 'Select time slot for exam' },
  status: {
    type: String,
    default: 'active',
    enum: ['active', 'inactive', 'deleted', 'started', 'completed', 'dropped']
  },
  streamUrl: { type: String },
  isExamStarted: { type: Boolean },
  examDuration: { type: String },
  logs: { type: Array },
  endDateTime: { type: Date },
  penaltyCharged: { type: Boolean, default: false },
  termsConditionsAccepted: { type: Boolean, default: false },
  termsConditionsAccepted2: { type: Boolean, default: false },
  termsConditionsAccepted3: { type: Boolean, default: false },
  userImage: { type: String },
  idImage: { type: String },
  userVerifiedStatus: {
    type: String,
    default: 'pending',
    enum: ['pending', 'non_verified', 'verified']
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: 'createdBy is required'
  },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  isFavourite: { type: Boolean, default: false },
  startTime:{type: Date},
  endTime:{type: Date},
})

examScheduleSchema.pre('save', function (next) {
  const now = Date.now()
  this.updatedAt = now
  // Set a value for createdAt only if it is null
  if (!this.createdAt) {
    this.createdAt = now
  }
  // Call the next function in the pre-save chain
  next()
})

const ExamSchedule = mongoose.model('ExamSchedule', examScheduleSchema)

async function addExamLogs (data, logs) {
  //console.log('data', 'logs', data, logs)
  return new Promise((resolve, reject) => {
    let updateCriteria = {}
    if (logs && typeof logs === 'string' && logs !== '') {
      var logsArray = logs.split(',')
      var filteredLogs = logsArray.filter(function (el) {
        if (el !== null || el !== '') return el.replace(/[']/g, '')
      })
    } else var filteredLogs = []

    if (data.camId !== undefined && data.camId !== '') {
      let datetime = new Date();
      updateCriteria = {
        $set: { streamUrl: data.camId, status: 'started', startTime:datetime},
        $push: { logs: filteredLogs }
      }
    } else {
      updateCriteria = {
        $push: { logs: filteredLogs }
      }
    }
    // console.log('data',data,'updateCriteria',updateCriteria)
    ExamSchedule.update(
      { examId: data.examId, userId: data.userId },
      updateCriteria,
      { upsert: true },
      (err, result) => {
        if (err) {
          return reject(err)
        } else {
          return resolve(result)
        }
      }
    )
  })
}

const getScheduledExamCount = examid => {
  return new Promise((resolve, reject) => {
    ExamSchedule.count({ examId: examid })
      .then(examCount => {
        resolve(examCount)
      })
      .catch(err => reject(err))
  })
}

const hasScheduledAlready = (examId, userId) => {
  return new Promise((resolve, reject) => {
    ExamSchedule.count({ examId: examId, userId: userId })
      .then(examCount => {
        resolve(examCount)
      })
      .catch(err => reject(err))
  })
}

exports.findById = id => {
  return new Promise((resolve, reject) => {
    ExamSchedule.findById({ _id: mongoose.Types.ObjectId(id) })
      .populate('examId')
      .populate('createdBy', 'name email username status')
      .populate('userId', 'name email username status')
      .lean()
      .exec((err, result) => {
        if (err) {
          reject(err)
        }
        resolve(result)
      })
  })
}
exports.updateScheduledExams = (
  findCriteria,
  updateCriteria,
  updateOne = true
) => {
  return new Promise((resolve, reject) => {
    if (updateOne) {
      ExamSchedule.findOneAndUpdate(findCriteria, updateCriteria)
        .lean()
        .exec((err, result) => {
          if (err) reject(err)
          else resolve(result)
        })
    } else {
      ExamSchedule.update(findCriteria, updateCriteria, { multi: true })
        .lean()
        .exec((err, result) => {
          if (err) reject(err)
          else resolve(result)
        })
    }
  })
}

exports.getByUserId = id => {
  return new Promise((resolve, reject) => {
    ExamSchedule.find({ userId: mongoose.Types.ObjectId(id) })
      .populate(
        [{ path: 'examId', select:'status name examFile examDuration courseId', match: { status: 'active'}, 
            populate: { path: 'courseId', select:'status name rulesIds courseId', match: { status: 'active'}, 
              populate:{path:'rulesIds', select:'status ruleTitle subRuleTitle description',match: { status: 'active' }} 
            }
        }])
      //.populate('examId', 'status name examFile examDuration courseId')
      .populate('createdBy', 'name email username isVerifiedImage status')
      .populate('userId', 'name email username isVerifiedImage status')
      .lean()
      .exec((err, result) => {
        if (err) {
          reject(err)
        } else {
          resolve(
            (result = result.filter(item => {
              return item.examId.status === 'active'
            }))
          )
        }
      })
  })
}

exports.list = (page, fields, filter = null) => {
  let perPage = 10
  let projectCriteria = {}
  let filterCriteria = { status: { $ne: 'deleted' } }
  return new Promise((resolve, reject) => {
    if (fields !== undefined && fields !== '') {
      let fieldsArray = fields.split(',')
      fieldsArray.map(field => {
        projectCriteria[field] = 1
      })
    }

    if (filter) {
      filterCriteria = { ...filterCriteria, ...filter }
    }
    if (page !== undefined && page >= 0) {
      ExamSchedule.find(filterCriteria, projectCriteria)
        .populate('examId', 'name status')
        .populate('userId', 'name email status')
        .populate('createdBy', 'name email username status')
        .limit(perPage)
        .skip(perPage * page)
        .sort({ createdAt: -1 })
        .lean()
        .exec(function (err, result) {
          if (err) {
            reject(err)
          } else {
            resolve(result)
          }
        })
    } else {
      // gives all exams without populate method
      ExamSchedule.find({ status: { $ne: 'deleted' } }, projectCriteria)
        .populate('examId', 'name status')
        .populate('userId', 'name email status')
        .populate('createdBy', 'name email username status')
        .sort({ createdAt: -1 })
        .lean()
        .exec(function (err, result) {
          if (err) {
            reject(err)
          } else {
            resolve(result)
          }
        })
    }
  })
}

exports.updateExamLogs = (updateCriteria, updateFields) => {
  return new Promise((resolve, reject) => {
    ExamSchedule.update(
      updateCriteria,
      { $set: updateFields },
      (err, result) => {
        if (err) {
          console.log('err', err)
          return reject(err)
        } else {
          console.log('result', result)
          return resolve(result)
        }
      }
    )
  })
}

exports.getLiveExamLogs = page => {
  let perPage = 10;
  return new Promise((resolve, reject) => {
    ExamSchedule.find(
      { status: 'started' },
      {
        examId: 1,
        logs: 1,
        updatedAt: 1,
        createdBy: 1,
        status: 1,
        streamUrl: 1,
        userId: 1,
        isFavourite: 1,
        startTime:1
      }
    )
      .populate('examId', 'name status examDuration')
      .populate('userId', 'name email username status userImage isFavourite')
      .limit(perPage)
      .skip(perPage * page)
      .sort({ createdAt: -1 })
      .lean()
      .exec(function (err, result) {
        if (err) {
          reject(err)
        } else {
          resolve(result)
        }
      })
  })
}
exports.getLiveExamLogById = data => {
  return new Promise((resolve, reject) => {
    ExamSchedule.findOne(
      { examId: data.examId, userId: data.userId, status: 'started' },
      {
        examId: 1,
        logs: 1,
        updatedAt: 1,
        createdBy: 1,
        status: 1,
        streamUrl: 1,
        userId: 1,
        isFavourite: 1,
        startTime:1
      }
    )
      .populate('examId', 'name status examDuration')
      .populate('userId', 'name email username status, isFavourite')
      .sort({ createdAt: -1 })
      .lean()
      .exec(function (err, result) {
        if (err) {
          reject(err)
        } else {
          resolve(result)
        }
      })
  })
}



exports.chatList = () => {
  return new Promise((resolve, reject) => {
    ExamSchedule.aggregate([
      {
        $group: {
          _id: '$userId',
          exams: { $push: { examId: '$examId', status: '$status'} }
        }
      },
      {$sort:{'exams.status':-1}},
      {
        $lookup: {
          from: 'users',
          localField: '_id',
          foreignField: '_id',
          as: 'users'
        }
      },
      {
        $match: {
          'users.status': 'active'
        }
      },
    
      {
        $project: {
          'users._id':1,
          'users.name': 1,
          'users.email': 1,
          'users.username': 1,
          'users.status': 1,
          'users.userImage': 1,
          'users.instituteId': 1,
          'users.isFavourite': 1,
          'exams':1
        }
      }
    ])
      .exec(function (err, result) {
        if (err) {
          reject(err)
        } else {
          resolve(result)
        }
      })
  })
}


exports.updateExamLogs = (updateCriteria, updateFields) => {
  return new Promise((resolve, reject) => {
    ExamSchedule.update(updateCriteria, { $set: updateFields }, (err, result) => {
      if (err) {
        return reject(err)
      } else {
        return resolve(result)
      }
    })
  })
}

exports.ExamSchedule = ExamSchedule
exports.getScheduledExamCount = getScheduledExamCount
exports.hasScheduledAlready = hasScheduledAlready

exports.addExamLogs = addExamLogs
