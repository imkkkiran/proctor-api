const mongoose = require('mongoose')
const { Schema } = mongoose

const roleSchema = new Schema({
  rolename: { type: String, unique: true },
  description: { type: String },
  status: { type: String },
  createdAt: { type: Date, default: Date.now },
  createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  updatedAt: { type: Date, default: Date.now }
})

roleSchema.pre('save', function (next) {
  const now = Date.now()
  this.updatedAt = now
  this.status = 'active'
  // Set a value for createdAt only if it is null
  if (!this.createdAt) {
    this.createdAt = now
  }
  // Call the next function in the pre-save chain
  next()
})

exports.findById = id => {
  return Role.findById(id).then(result => {
    result = result.toJSON()
    delete result._id
    delete result.__v
    return result
  })
}

exports.list = (perPage, page) => {
  return new Promise((resolve, reject) => {
    Role.find({ status: { $ne: 'deleted' } })
    .populate('createdBy', 'name email username status')
    .limit(perPage).skip(perPage * page).lean().exec(function (err, roles) {
      if (err) {
        reject(err)
      } else {
        resolve(roles)
      }
    })
  })
}

exports.patchRole = (id, roleData) => {
  return new Promise((resolve, reject) => {
    Role.findById(id, function (err, role) {
      if (err) reject(err)
      for (const i in roleData) {
        role[i] = roleData[i]
      }

      role.save(function (err, updatedRole) {
        if (err) return reject(err)
        resolve(updatedRole)
      })
    })
  })
}

exports.removeById = (roleId, status = 'deleted') => {
  return new Promise((resolve, reject) => {
    Role.updateOne({ _id: roleId }, { $set: { status: status } }, (err, result) => {
      if (err) {
        reject(err)
      }
      resolve(result)
    })
  })
}

const Role = mongoose.model('Role', roleSchema)

exports.Role = Role
