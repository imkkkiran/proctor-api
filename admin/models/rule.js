const mongoose = require('mongoose')
const { Schema } = mongoose

const ruleSchema = new Schema({
  ruleTitle: { type: String, required: 'Rule Title is required', unique: true },
  subRuleTitle: { type: String, required: 'Sub Rule Title is required' },
  description: { type: String },
  status: { type: String, default: 'active', enum: ['active', 'inactive', 'deleted'] },
  createdAt: { type: Date, default: Date.now },
  createdBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: 'CreatedBy field is required' },
  updatedBy: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: 'updatedBy field is required' },
  updatedAt: { type: Date, default: Date.now }
})

const Rule = mongoose.model('Rule', ruleSchema)

ruleSchema.pre('save', function (next) {
  const now = Date.now()
  this.updatedAt = now
  // Set a value for createdAt only if it is null
  if (!this.createdAt) {
    this.createdAt = now
  }
  // Call the next function in the pre-save chain
  next()
})

exports.list = (perPage, page) => {
  return new Promise((resolve, reject) => {
    Rule.find({ status: { $ne: 'deleted' } })
    .populate('createdBy', 'name email username status')
    .limit(perPage).skip(perPage * page).sort({ createdAt: -1 }).lean().exec(function (err, rules) {
      if (err) {
        reject(err)
      } else {
        resolve(rules)
      }
    })
  })
}

exports.listForModule = (perPage, page) => {
  return new Promise((resolve, reject) => {
    Rule.find({ status: { $eq: 'active' } })
    .populate('createdBy', 'name email username status')
    .limit(perPage).skip(perPage * page).sort({ createdAt: -1 }).lean().exec(function (err, rules) {
      if (err) {
        reject(err)
      } else {
        resolve(rules)
      }
    })
  })
}

exports.removeById = (ruleid, status = 'deleted') => {
  console.log({ _id: ruleid })
  return new Promise((resolve, reject) => {
    Rule.updateOne({ _id: ruleid }, { $set: { status: status } }, (err, result) => {
      if (err) {
        reject(err)
      }
      resolve(result)
    })
  })
}


exports.findById = id => {
  return new Promise((resolve, reject) => {
    Rule.findById({ _id: mongoose.Types.ObjectId(id) })
    .populate('createdBy', 'name email username status')
    .lean().exec((err, result) => {
        if (err) {
          reject(err)
        }
        resolve(result)
      })
  })
}
exports.patchRule = (id, ruleData) => {
  return new Promise((resolve, reject) => {
    Rule.findById(id, function (err, rule) {
      if (err) reject(err)
      for (const i in ruleData) {
        rule[i] = ruleData[i]
      }

      rule.save(function (err, updatedRule) {
        if (err) return reject(err)
        resolve(updatedRule)
      })
    })
  })
}
exports.Rule = Rule
