const mongoose = require('mongoose')

const studentExamLog = mongoose.Schema({
  examId: { type:mongoose.Schema.Types.ObjectId, required: 'ExamId is required' },
  userId: { type: mongoose.Schema.Types.ObjectId, required: 'UserID is required' },
  examStreamUrl: { type: String},
  examStatus: { 
      type: String,
      default: 'P',
      enum: ['P', 'S', 'C']
  },
  incidentLogs: { type: String},
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now }
})

studentExamLog.pre('save', function (next) {
  const now = Date.now()
  this.updatedAt = now
  if (!this.createdAt) {
    this.createdAt = now
  }
  next()
})

const Exam = mongoose.model('Exam', studentExamLog)

exports.list = (page, fields) => {
  let perPage = 10
  let projectCriteria = {}
  return new Promise((resolve, reject) => {
    if (fields !== undefined && fields !== '') {
      fieldsArray = fields.split(',')
      fieldsArray.map(field => {
        projectCriteria[field] = 1
      })
    }

    if (page !== undefined && page >= 0) {
      Exam.find({ status: { $ne: 'C' } }, projectCriteria)
        .limit(perPage)
        .skip(perPage * page)
        .sort({ createdAt: -1 })
        .lean()
        .exec(function (err, result) {
          if (err) {
            reject(err)
          } else {
            resolve(result)
          }
        })
    } else {
      // gives all exams without populate method
      Exam.find({ status: { $ne: 'C' } }, projectCriteria)
        .sort({ createdAt: -1 })
        .lean()
        .exec(function (err, result) {
          if (err) {
            reject(err)
          } else {
            resolve(result)
          }
        })
    }
  })
}




exports.StudentExam = Exam
