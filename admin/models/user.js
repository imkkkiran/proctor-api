const mongoose = require('mongoose')
const crypto = require('crypto')
const config = require('./../../config/config')
const jwt = require('jsonwebtoken')

var validateEmail = function (email) {
  var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/
  return re.test(email)
}
const userSchema = mongoose.Schema({
  name: { type: String, required: 'Name is required' },
  //firstName: { type: String, required: 'First Name is required' },
  lastName: { type: String, required: 'Last Name is required' },
  instituteId: { type: String },
  instituteName: { type: String },
  grade: { type: String },
  // username: { type: String, unique: true, required: 'Username is required' },
  username: { type: String },
  email: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    required: 'Email address is required',
    validate: [validateEmail, 'Please fill a valid email address'],
    match: [
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      'Please fill a valid email address'
    ]
  },
  salt: { type: String },
  password: { type: String, required: 'password field is required' },
  status: {
    type: String,
    default: 'active',
    enum: ['active', 'inactive', 'deleted']
  },
  createdBy: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: 'createdBy field is required'
  },
  createdAt: { type: Date, default: Date.now },
  updatedAt: { type: Date, default: Date.now },
  roleId: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Role',
    required: 'createdBy field is required'
  },
  userImage: {
    type: String,
    required: 'User image is required.'
  },
  userIdCard: {
    type: String,
    required: 'User image is required.'
  },
  isVerifiedImage: {
    type: String,
    default: 'P',
    enum: ['P', 'A', 'R'] //P-pending, A-Approve, R-rejected
  },
  imageApproveComment: {
    type: String
  },
  isFavourite: { type: Boolean, default: false },
  resetPasswordToken:{type:String, default:false},
  resetPasswordExpires:{type:String, default:false}
  // roleId: {
  //   type: String,
  //   default: 'PROCTOR',
  //   enum: ['STUDENT', 'PROCTOR', 'ADMIN']
  // }
})

userSchema.pre('save', function (next) {
  const now = Date.now()
  this.updatedAt = now
  // Set a value for createdAt only if it is null
  if (!this.createdAt) {
    this.createdAt = now
  }
  // Call the next function in the pre-save chain
  next()
})

const validateLogin = data => {
  if (!data.email || data.email === '') {
    return { error_code: 1, message: 'Email is required', success: false }
  } else if (!validateEmail(data.email)) {
    return { error_code: 2, message: 'Email is not valid', success: false }
  } else if (!data.password || data.password === '') {
    return { error_code: 3, message: 'Password is required', success: false }
  } else {
    return { success: true }
  }
}

const login = data => {
  const { email, password } = data

  return new Promise((resolve, reject) => {
    const validation = validateLogin(data)
    if (validation.success) {
      User.findOne(
        { email: email },
        {
          email: 1,
          _id: 1,
          username: 1,
          name: 1,
          roleId: 1,
          password: 1,
          salt: 1,
          status: 1,
          isVerifiedImage: 1,
          userImage: 1,
          userIdCard: 1,
          imageApproveComment: 1
        }
      )
        .populate('roleId')
        .lean()
        .exec((err, doc) => {
          if (err) {
            return reject(err.toString())
          } else {
            if (doc) {
              if (doc.status != 'active')
                return reject('Account deactivated/deleted')

              const salt = doc.salt
              const dbpassword = doc.password
              const hash = crypto
                .createHmac('sha256', salt)
                .update(password)
                .digest('base64')
              const inputpassword = salt + '$' + hash
              if (dbpassword === inputpassword) {
                const token = jwt.sign({ sub: doc._id }, config.secret, {
                  expiresIn: config.expiresIn
                })
                return resolve({
                  data: {
                    _id: doc._id,
                    name: doc.name,
                    username: doc.username,
                    email: doc.email,
                    name: doc.name,
                    roleId: doc.roleId._id,
                    roleName: doc.roleId.rolename.toString(),
                    isVerifiedImage: doc.isVerifiedImage,
                    userImage: doc.userImage,
                    userIdCard: doc.userIdCard,
                    imageApproveComment: doc.imageApproveComment,
                    token: token
                  }
                })
              } else {
                return reject('Incorrect username or password')
              }
            } else {
              return reject('Incorrect username or password')
            }
          }
        })
    } else {
      return reject(validation.message)
    }
  })
}
exports.findById = id => {
  return new Promise((resolve, reject) => {
    User.findById(
      { _id: mongoose.Types.ObjectId(id) },
      { salt: 0, password: 0}
    )
      .populate('roleId')
      .exec((err, result) => {
        if (err) {
          reject(err)
        }
        delete result.__v
        delete result.salt
        delete result.password
        const data = result
        resolve(data)
      })
  })
}

const patchUser = (id, userData) => {
  return new Promise((resolve, reject) => {
    User.findById(id, function (err, user) {
      if (err) reject(err)
      for (const i in userData) {
        user[i] = userData[i]
      }
      user.save(function (err, updatedUser) {
        if (err) return reject(err)
        resolve(updatedUser)
      })
    })
  })
}

exports.list = (perPage, page, fields) => {
  let projectCriteria = { }
  return new Promise((resolve, reject) => {
    if (fields !== undefined && fields !== '') {
      fieldsArray = fields.split(',')
      fieldsArray.map(field => {
        projectCriteria[field] = 1
      })
    }
    User.find({ status: { $ne: 'deleted' } }, projectCriteria)
      .populate('roleId')
      .limit(perPage)
      .skip(perPage * page)
      .sort({ createdAt: -1 })
      .lean()
      .exec(function (err, users) {
        if (err) {
          console.log(err)
          reject(err)
        } else {
          resolve(users)
        }
      })
  })
}

exports.removeById = (userId, status = 'deleted') => {
  return new Promise((resolve, reject) => {
    User.update(
      { _id: userId },
      { $set: { status: status } },
      (err, result) => {
        if (err) {
          reject(err)
        }
        resolve(result)
      }
    )
  })
}
const User = mongoose.model('User', userSchema)

exports.verifyPassword = data => {
  return new Promise((resolve, reject) => {
    User.findOne({ _id: data.userId }, { _id: 1, password: 1, salt: 1 })
      .lean()
      .exec()
      .then(doc => {
        if (doc) {
          const salt = doc.salt
          const dbpassword = doc.password
          const hash = crypto
            .createHmac('sha256', salt)
            .update(data.oldPassword)
            .digest('base64')
          const inputpassword = salt + '$' + hash
          if (dbpassword === inputpassword) {
            return resolve('User verified')
          } else {
            return reject('Incorrect old password')
          }
        } else {
          return reject('User not found')
        }
      })
      .catch(err => {
        reject(err.toString())
      })
  })
}

exports.generatePasswordReset = function () {
  return {
    resetPasswordToken: crypto.randomBytes(20).toString('hex'),
    resetPasswordExpires: Date.now() + 3600000
  }
}

exports.userDetails = (userId) => {
  return new Promise((resolve, reject) => {
    User.aggregate([
      {
        $match:  { '_id' :mongoose.Types.ObjectId(userId) ,'status': 'active' }
      },
      {
        $lookup: {
          from: 'examschedules',
          localField: '_id',
          foreignField: 'userId',
          as: 'exams'
        }
      },
      {
        $project: {
          '_id':1,
          'name': 1,
          'email': 1,
          'username': 1,
          'status': 1,
          'userImage': 1,
          'instituteId': 1,
          'instituteId': 1,
          'instituteName': 1,
          'userIdCard':1,
          'exams.status':1,
          'exams.examId':1
        }
      }
    ])
      .exec(function (err, result) {
        if (err) {
          console.log(err)
          reject(err)
        } else {
          if(result)
          resolve(result[0])
          else reject('No data found')
        }
      })
  })
}

exports.updateFav = (userId, favStatus) => {
  return new Promise((resolve, reject) => {
    User.update(
      { _id: userId },
      { $set: { isFavourite: favStatus } },
      (err, result) => {
        if (err) {
          return reject(err)
        } else {
          return resolve(result)
        }
      }
    )
  })
}

exports.User = User
exports.login = login
exports.patchUser = patchUser
