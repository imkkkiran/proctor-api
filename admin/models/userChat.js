const mongoose = require('mongoose')

const userChatSchema = mongoose.Schema({
    roomId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    message: {
        type: String
    },
    nickname: {
        type: String
    },
    msgTime:{
        type: String
    },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
})

userChatSchema.pre('save', function (next) {
  const now = Date.now()
  this.updatedAt = now
  // Set a value for createdAt only if it is null
  if (!this.createdAt) {
    this.createdAt = now
  }
  // Call the next function in the pre-save chain
  next()
})

const UserChat = mongoose.model('UserChat', userChatSchema)
addChatLogs = (data, logs) => {
    return new Promise((resolve, reject) => {
      let updateCriteria = {}  
      updateCriteria = {
        message:data.message,
        roomId:data.roomId,
        nickname:data.nickname,
        msgTime:data.msgTime
      }
      const UserChat = new UserChat.UserChat(examBody)
      return UserChat.save()
    })
  }

  exports.findById = id => {
    return new Promise((resolve, reject) => {
      UserChat.find({ roomId: mongoose.Types.ObjectId(id) })
        .lean()
        .sort({ createdAt: 1 })
        .exec((err, result) => {
          if (err) {
            reject(err)
          }
          resolve(result)
        })
    })
  }
  exports.UserChat = UserChat
  exports.addChatLogs = addChatLogs
  