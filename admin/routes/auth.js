var express = require('express')
var router = express.Router()
const authCtrl = require('./../controllers/auth')

router.post('/', authCtrl.authenticate)

router.get('/testauth', authCtrl.getAll)

module.exports = router
