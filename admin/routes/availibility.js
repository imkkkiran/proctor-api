const availController = require('./../controllers/availibility')

const express = require('express')
const router = express.Router()

router.post('/', [availController.insert])
router.delete('/:id/:status', [availController.removeById])
router.get('/', [availController.list])
router.patch('/:id', [availController.patchById])
module.exports = router
