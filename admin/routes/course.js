const CourseController = require('./../controllers/course')

const express = require('express')
const router = express.Router()

router.post('/', [CourseController.insert])
router.delete('/:courseid/:status', [CourseController.removeById])
router.get('/:courseid', [CourseController.getById])
router.patch('/:courseid', [CourseController.patchById])
router.get('/', [CourseController.list])

module.exports = router
