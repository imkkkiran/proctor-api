const controller = require('./../controllers/dashboard')
const express = require('express')
const router = express.Router()
router.get('/count', [controller.getCount])
router.get('/user-count/:userId', [controller.getCount])
module.exports = router
