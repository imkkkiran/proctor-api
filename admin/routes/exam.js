const ExamController = require('./../controllers/exam')
const express = require('express')
const router = express.Router()
const multer = require('multer')
// const filemw = require('./../middlewares/uploadfile')
/*
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, './uploads/')
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})
/*
const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpg' || file.mimetype === 'image/png') {
    cb(null, true)
  } else {
    cb(null, false)
  }
}
*/
/*
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  }
  // fileFilter: fileFilter
}) */
// router.post('/', filemw.uploadSingle('examfile', './uploads/exams/', /jpeg|jpg|png|html/), [ExamController.insert])
router.post('/', [ExamController.insert])
router.delete('/:examid/:status', [ExamController.removeById])
router.get('/', [ExamController.list])
router.get('/courses_exam', [ExamController.usersCourseWithExam])
router.patch('/:examid', [ExamController.patchById])
router.get('/coursesexam', [ExamController.usersCourseExam])
module.exports = router
