var express = require('express')
var router = express.Router()
const Ctrl = require('./../controllers/examLogs')

router.get('/liveExam', Ctrl.getLiveExamLogs)
router.post('/updateExamStatus', Ctrl.updateExamLogStatus)
module.exports = router
