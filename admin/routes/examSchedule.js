const ExamScheduleController = require('./../controllers/examSchedule')
const express = require('express')
const router = express.Router()

router.post('/', [ExamScheduleController.schedule])
router.get('/schedule/:examscheuleid', [ExamScheduleController.getById])
router.get('/userschedule/:examscheuleid', [ExamScheduleController.getByUserId])
router.delete('/:examscheuleid', [ExamScheduleController.removeById])
router.patch('/:examscheuleid', [ExamScheduleController.patchById])
router.get('/liveExam', ExamScheduleController.getLiveExamLogs)
router.post('/list', [ExamScheduleController.list])

router.get('/chatlist', [ExamScheduleController.chatlist])
router.post('/updateExamStatus', [ExamScheduleController.updateExamLogStatus])
module.exports = router