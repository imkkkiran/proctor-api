const roleController = require('./../controllers/role')

const express = require('express')
const router = express.Router()

router.post('/addRole', [roleController.insert])
router.delete('/:roleid/:status', [roleController.removeById])
router.get('/roleList', [roleController.list])
router.patch('/:roleid', [roleController.patchById])
module.exports = router
