const ruleController = require('./../controllers/rule')

const express = require('express')
const router = express.Router()

router.post('/add-rule', [ruleController.insert])
router.delete('/:ruleid/:status', [ruleController.removeById])
// router.get('/:id', [roleController.insert])
router.get('/', [ruleController.list])
router.get('/listForModule', [ruleController.listForModule])

router.get('/:ruleid', [ruleController.getById])
router.patch('/:ruleid', [ruleController.patchById])
module.exports = router
