var express = require('express')
var router = express.Router()
const streamProcess = require('./../controllers/streamProcess')

router.post('/', streamProcess.objectDet)

module.exports = router
