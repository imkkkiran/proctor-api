const UsersController = require('./../controllers/user')

const express = require('express')
const router = express.Router()

router.post('/login', [UsersController.login])
router.post('/', [UsersController.insert])
router.delete('/:userid/:status', [UsersController.removeById])
router.get('/:userid', [UsersController.getById])
router.patch('/:userid', [UsersController.patchById])
router.get('/', [UsersController.list])
router.post('/register', [UsersController.insert])
router.post('/change_password', [UsersController.changePassword])
router.post('/recover', [UsersController.recover])
router.post('/verify_resest_link', [UsersController.reset])
router.post('/reset_password', [UsersController.resetPassword])
router.post('/dashboard_counts', [UsersController.dashboardCounts])
router.get('/userDetails/:userid', [UsersController.userDetails])
router.post('/updateFav', [UsersController.updateFav])

module.exports = router
