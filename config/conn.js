const mongoose = require('mongoose')

// Set up mongoose connection on mongoAtlas
const dev_db_url = 'mongodb://localhost/proctor-api'

// connects our back end code with the database
mongoose.connect(process.env.MONGODB_URI || dev_db_url, { useNewUrlParser: true,useUnifiedTopology: true, useCreateIndex: true });

const db = mongoose.connection

db.once('open', () => console.log('Connected to the Mongodb database'))

// checks if connection with the database is successful
db.on('error', (error) => console.log('Mongodb connection failed'))
