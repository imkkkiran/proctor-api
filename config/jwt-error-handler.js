module.exports = errorHandler

function errorHandler (err, req, res, next) {
  if (typeof (err) === 'string') {
    // custom application error
    return res.status(400).json({ success: false, message: err, error_code: 401, data: {} })
  }

  if (err.name === 'UnauthorizedError') {
    // jwt authentication error
    return res.status(401).json({ success: false, message: 'Invalid Token', error_code: 401, data: {} })
  }

  // default to 500 server error
  console.log(err)
  return res.status(500).json({ success: false, message: err.message, error_code: 500, data: {} })
}
