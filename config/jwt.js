const expressJwt = require('express-jwt')
const config = require('./config.json')

module.exports = jwt

function jwt () {
  const { secret } = config
  return expressJwt({ secret }).unless({
    path: [
      // public routes that don't require authentication
      '/auth',
	  '/users/login',
      '/users/register',
      '/users/recover',
      '/users/reset_password',
	  '/users/verify_resest_link',
      '/socket'
    ]
  })
}
