var nodemailer = require('nodemailer')
var smtpTransport = require('nodemailer-smtp-transport')
const env = process.env.NODE_ENV || 'development'
const config = require(`./${env}.js`)

var transporter = nodemailer.createTransport(
  smtpTransport({
    service: 'gmail',
    host: 'smtp.gmail.com',
    auth: {
      user: config.GMAIL_USERNAME,
      pass: config.GMAIL_PASSWORD
    }
  })
)

exports.sendMail = (mailOptions) => {
  return new Promise((resolve, reject) => {
    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        reject(error)
      } else {
        resolve('Email sent: ' + info.response)
      }
    })
  })
}
