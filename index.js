/* Modules imported */
const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const logger = require('morgan')
const cors = require('cors')
const jwt = require('./config/jwt')
const errorHandler = require('./config/jwt-error-handler')
const fileUpload = require('express-fileupload')
const env = process.env.NODE_ENV || 'development';
const UserChatModel = require('./admin/models/userChat');
require('./config/conn')
/* Variables & constants declared */
var app = express()
const server = require('http').Server(app)
const https = require('https')
var privateKey = ''
var certificate = ''
const credentials = { key: privateKey, cert: certificate }
const httpsServer = https.createServer(credentials, app)
var spawn = require('child_process').spawn
global.io = require('socket.io')(server)
app.use(express.static(__dirname + 'public'));
global.__basedir = __dirname;

spawn('ffmpeg', ['-h']).on('error', function (m) {
  console.error(
    'FFMpeg not found in system cli; please install ffmpeg properly or make a softlink to ./!'
  )
  process.exit(-1)
})

io.on('connection', function (socket) {

  socket.emit('message', 'Hello from mediarecorder-to-rtmp server!')
  socket.emit('message', 'Please set rtmp destination before start streaming.')

  var ffmpeg_process,
  feedStream = false
  socket.on('config_rtmpDestination', function (m) {
    if (typeof m != 'string') {
      socket.emit('fatal', 'rtmp destination setup error.')
      return
    }
    var regexValidator = /^rtmp:\/\/[^\s]*$/ //TODO: should read config
    if (!regexValidator.test(m)) {
      socket.emit('fatal', 'rtmp address rejected.')
      return
    }
    socket._rtmpDestination = m
    console.log('socket._rtmpDestination', m);
    socket.emit('message', 'rtmp destination set to:' + m)
  })
  socket._vcodec='libvpx';//from firefox default encoder
  socket.on('config_vcodec', function (m) {
    if (typeof m != 'string') {
      socket.emit('fatal', 'input codec setup error.')
      return
    }
    if (!/^[0-9a-z]{2,}$/.test(m)) {
      socket.emit('fatal', 'input codec contains illegal character?.')
      return
    } //for safety
    socket._vcodec = m
  })

  socket.on('start', function (m) {
    if (ffmpeg_process || feedStream) {
      socket.emit('fatal', 'stream already started.')
      return
    }
    if (!socket._rtmpDestination) {
      socket.emit('fatal', 'no destination given.')
      return
    }

    var ops = [
      '-y',
	  '-i',
	  '-c:a',
	  'aac',
	  '-strict',
	  '-2',
	  '-b:a',
	  '128k',
	  '-c:v',
	  'libx264',
      '-f',
      'flv',
      socket._rtmpDestination
    ]

    console.log('from server js ',socket._rtmpDestination)
    var ffmpeg_process = spawn('ffmpeg', ops)
    feedStream = function (data) {
      ffmpeg_process.stdin.write(data)
      //write exception cannot be caught here.
    }

    ffmpeg_process.stderr.on('data', function (d) {
      socket.emit('ffmpeg_stderr', '' + d)
    })
    ffmpeg_process.on('error', function (e) {
      console.log('child process error' + e)
      socket.emit('fatal', 'ffmpeg error!' + e)
      feedStream = false
      socket.disconnect()
    })
    ffmpeg_process.on('exit', function (e) {
      console.log('child process exit' + e)
      socket.emit('fatal', 'ffmpeg exit!' + e)
      socket.disconnect()
    })
  })

  socket.on('binarystream', function (m) {
    if (!feedStream) {
      socket.emit('fatal', 'rtmp not set yet.')
      ffmpeg_process.stdin.end()
      ffmpeg_process.kill('SIGINT')
      return
    }
    feedStream(m)
  })
 
  socket.on('disconnect', function () {
    feedStream = false
    if (ffmpeg_process)
      try {
        ffmpeg_process.stdin.end()
        ffmpeg_process.kill('SIGINT')
      } catch (e) {
        console.warn('killing ffmoeg process attempt failed...')
      }
  })
  socket.on('error', function (e) {
    console.log('socket.io error:' + e)
  })
})
io.on('error', function (e) {
  console.log('socket.io error:' + e)
})


io.on("connection", socket => {
	const { id } = socket.client;
	//console.log(`User connected: ${id}`);
  
  
  socket.on('chatroom', chatRoom => {
    console.log("chatRoom joing from chatRoom==", chatRoom);
    socket.join(chatRoom);
    //io.emit('chatroom', chatRoom)
  })

	socket.on("chatMessage", ({ chatroom, nickname, msg }) => {
  //  console.log(`${id}: ${chatroom}:${nickname}:${msg}`);
    let datetime = new Date().toLocaleTimeString();
    let data = {message:msg,nickname:nickname,roomId:chatroom,msgTime:datetime};
    const userChat = UserChatModel.UserChat(data);
    userChat.save();
    io.sockets.in(chatroom).emit("chatMessage", {chatroom, nickname, msg});
	  //io.emit("chatMessage", {chatroom, nickname, msg});
  });	
});


/* Middlewares added */
app.use('/public', express.static(path.join(__dirname, 'public')))
app.set('views', './views')
app.set('view engine', 'ejs')

app.use(cors())
app.use(fileUpload())
app.use(logger('dev'))
app.use(express.json({ limit: '50mb' }))
app.use(express.urlencoded({ extended: false, limit: '50mb' }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(jwt())

/* Routes added */
app.use('/uploads', express.static('uploads'))
app.use('/users', require('./admin/routes/user'))
app.use('/courses', require('./admin/routes/course'))
app.use('/auth', require('./admin/routes/auth'))
app.use('/role', require('./admin/routes/role'))
app.use('/rule', require('./admin/routes/rule'))
app.use('/exam', require('./admin/routes/exam'))
app.use('/schedule', require('./admin/routes/examSchedule'))
app.use('/availibility', require('./admin/routes/availibility'))
app.use('/', require('./admin/routes/ai'))
app.use('/dashboard', require('./admin/routes/dashboard'))
app.use(errorHandler)

const nodeMediaServer = require('./config/node_media_server')
nodeMediaServer.run()
// and call run() method at the end
// file where we start our web server
app.get('/live', function (req, res) {
 res.send(nodeMediaServer.run())
})

/* Server started */
if (env === 'development') {
  server.listen('5000', function () {
    const serverName = env || 'development'
    console.log(serverName, 'Server running on localhost:5000')
  })
} else {
  httpsServer.listen('5050', function () {
    const serverName = env || 'production'
    console.log(serverName, 'Server running on localhost:5050')
  })
}
