var express = require('express')
var app = express()

const server = require('http').Server(app)
const io = require('socket.io')(server)

io.on('connection', function (socket) {
  console.log('New Client is Connected!')

  socket.on('VerifyUser', function (data) {
    console.log('Recieced in python server verifyUser Event data', data)
    socket.emit('VerifyUserResponse', { success: false, data: '3rd person detected' })
  })

  socket.on('shareScreen', function (data) {
    console.log('Recieced in python server shareScreen Event data', data)
    // Process sharedScreen data & return response
    socket.emit('sharedScreenResponse', { success: false, data: '3rd person detected' })
  })
})

server.listen('7000', function () {
  console.log('Python Server running on localhost:7000')
})
